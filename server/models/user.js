import mongoose, { Schema } from 'mongoose';

export const UserSchema = new Schema(
  {
    email: {
      type: String,
      lowercase: true,
      trim: true,
      unique: true,
      required: true
    },
    password: {
      type: String,
      required: true,
    },
    reminderIsSet: {
      type: Boolean,
      default: false
    }
  },
  {
    collection: 'users',
    timestamps: true
  }
);

UserSchema.virtual('id').get(function () {
  return this._id.toHexString();
});

UserSchema.set('toJSON', {
  virtuals: true
});

export default mongoose.model('User', UserSchema);