import mongoose, { Schema } from 'mongoose';

export const EntrySchema = new Schema(
  {
    author: {
      type: Schema.Types.ObjectId, ref: 'User'
    },
    title: {
      type: String,
      trim: true,
      required: true
    },
    content: {
      type: String,
      required: true
    },
    isFavorite: {
      type: Boolean,
      default: false
    }
  },
  {
    collection: 'entries',
    timestamps: true
  }
);

EntrySchema.virtual('id').get(function () {
  return this._id.toHexString();
});

EntrySchema.set('toJSON', {
  virtuals: true
});

export default mongoose.model('Entry', EntrySchema);