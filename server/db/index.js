import mongoose from 'mongoose';

import dotenv from 'dotenv';

dotenv.config();

mongoose.Promise = global.Promise;

const connection = mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true });

connection
  .then(db => {
    console.info(
      `Successfully connected to ${process.env.DATABASE_URL} MongoDB cluster`,
    );
    return db;
  })
  .catch(err => {
    if (err.message.code === 'ETIMEDOUT') {
      console.info('Attempting to re-establish database connection.');
      mongoose.connect(process.env.DATABASE_URL);
    } else {
      console.error('Error while attempting to connect to database:');
      console.error(err);
    }
  });

export default connection;