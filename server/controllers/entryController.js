import 'babel-polyfill';
import { Error as MongoError} from 'mongoose';
import { Entry } from '../models';
import { entry as entryDto, user as userDto } from '../models/dto';
import validate from '../utils/validate';

class entryController {
  /**
   * Get all of a user's diary entries
   * Requires auth token to be passed in authorization header
   * @static
   * @param {*} req - request object
   * @param {*} res - response object
   * @returns {object} json
   * @memberof userController
   */
  static async getAllEntries(req, res, next) {
    try {
      // get query params
      let { limit, page } = req.query;
      const { filter } = req.query;
      const { userId } = req.authorizedUser;

      limit = limit ? parseInt(limit, 10) : 20;
      page = page ? parseInt(page, 10) : 0;
      const favs = filter && filter === 'favs';

      // get entries
      const start = limit * page;
      const options = Object.assign({}, { author: userId }, (favs && { isFavorite: true }));
      const userEntries = await Entry.find(options, entryDto)
        .populate('author', userDto)
        .limit(limit)
        .skip(start)
        .exec();
      res.status(200).json({
        entries: userEntries,
        meta: { limit, page },
      });
    } catch (error) {
      next(error);
    }
  }

  /**
   * @description Creates a new entry by a user
   *
   * @static
   * @param {*} req - Request object with title, content, is_favorite properties
   * @param {*} res - Response object
   * @returns {object} json
   * @memberof entryController
   */
  static async addEntry(req, res, next) {
    try {
      const { title, content, isFavorite } = req.body;
      const { userId } = req.authorizedUser;

      // add entry to database
      const newEntry = new Entry({
        title,
        content,
        isFavorite,
        author: userId
      })
      await newEntry.save();
      res.status(201).json(newEntry);
    } catch (error) {
      next(error);
    }
  }

  /**
   * @description Fetches a single user entry by id
   *
   * @static
   * @param {*} req - Request object with param 'id'
   * @param {*} res - Response object
   * @memberof entryController
   */
  static async getEntry(req, res, next) {
    const { params: { id }, authorizedUser: { userId } } = req;
    try {
      const entry = await Entry.findOne({ _id: id, author: userId }, entryDto)
        .populate('author', userDto).exec();
      if (!entry) {
        return res.status(404).json({ error: { message: 'Entry not found' } });
      }
      res.status(200).json(entry);
    } catch (error) {
      next(error);
    }
  }

  /**
   * @description Modifies a previously created entry not later than 24 hours
   *
   * @static
   * @param {*} req - Request object
   * @param {*} res - Response object
   * @returns {obj} json
   * @memberof entryController
   */
  static async modifyEntry(req, res, next) {
    try {
      const {
        params: { id },
        authorizedUser: { userId },
      } = req;
      let { body: { isFavorite } } = req;

      // convert them to null if they are undefined
      isFavorite = validate.booleanOrNull(isFavorite);
      const fields = Object.assign({}, req.body, ((isFavorite === null) && { isFavorite }));

      // check if entry exists and is already older than a day
      const entry = await Entry.findOne({ _id: id, author: userId }, entryDto).exec();
      if (!entry) {
        return res.status(404).json({ error: { message: 'Entry not found' } });
      }

      // older than a day?
      if (!validate.isWithinLast24hours(entry.createdAt)) {
        return res.status(403).json({ error: { message: 'Cannot update entry after 24 hours' } });
      }
      await Entry.updateOne(
        { _id: id, author: userId },
        fields
      );
      res.status(200).json(entry);
    } catch (error) {
      next(error);
    }
  }

  /**
   * @description Deletes a single user entry by id
   *
   * @static
   * @param {*} req - Request object with param 'id'
   * @param {*} res - Response object
   * @memberof entryController
   */
  static async deleteEntry(req, res, next) {
    const { params: { id }, authorizedUser: { userId } } = req;
    try {
      const deleted = await Entry.deleteOne({ _id: id, author: userId }).exec();
      res.status(204).json();
    } catch (error) {
      if (error instanceof MongoError.CastError) {
        return res.status(404).json({ error: { message: 'Entry not found' } });
      }
      next(error);
    }
  }
}

export default entryController;
