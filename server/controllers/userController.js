import 'babel-polyfill';
import bcrypt from 'bcrypt';
import { User, Entry } from '../models';
import signAuthToken from '../utils/signAuthToken';

class userController {
  /**
   * Creates a new user account
   * Requires email and password to be passed in request body
   * @static
   * @param {*} req - Client request object
   * @param {*} res - Server response object
   * @returns {object} token
   * @memberof userController
   */
  static async createUser(req, res, next) {
    try {
      // get email and password in request body
      const { email, password } = req.body;

      // check if user already exists - 409
      const user = await User.findOne({ email }).exec();
      if (user) {
        res.status(409).json({ error: { message: 'User already exists. Please login.' } });
      }

      // hash the password
      const passwordHash = await bcrypt.hash(password, 5);

      // insert new user into table, returning data
      const newUser = new User({ email, password: passwordHash });
      await newUser.save();

      const data = {
        userId: newUser._id,
        createdOn: user.createdAt,
      };
      const token = signAuthToken(data);

      // signed token - 201
      res.status(201).json({ token });
    } catch (error) {
      next(error);
    }
  }

  /**
   * Logs in an existing user
   * Requires email and password to be passed in request body
   * @static
   * @param {*} req - Client request object
   * @param {*} res - Server response object
   * @returns {token}
   * @memberof userController
   */
  static async loginUser(req, res, next) {
    try {
      // get email and password in request body
      const { email, password } = req.body;

      // fetch user
      const user = await User.findOne({ email }).exec();

      // if error in fetch, user does not exist - 422
      if (!user) {
        res.status(422).json({ error: { message: 'Email or Password is incorrect' } });
      }

      // check password
      const passwordIsValid = await bcrypt.compare(password, user.password);
      if (!passwordIsValid) {
        res.status(422).json({ error: { message: 'Email or Password is incorrect' } });
        return;
      }

      // create token
      const data = {
        userId: user._id, createdOn: user.createdAt,
      };
      const token = signAuthToken(data);
      res.status(200).json({ token });
    } catch (error) {
      next(error);
    }
  }

  /**
   * @description Fetches user profile
   * Returns user profile information
   * @static
   * @param {*} req - Request object
   * @param {*} res - Response object
   * @returns {data} email, entriesCount
   * @memberof userController
   */
  static async getProfile(req, res, next) {
    try {
      const user = await User.findOne({ email: req.authorizedUser.email }).exec();
      const allEntries = await Entry.find({ userId: user.id }).exec();
      const favEntries = await Entry.find({ userId: user.id, isFavorite: true }).exec();
      res.status(200).json({
        email: req.authorizedUser.email,
        entries_count: allEntries.length,
        fav_count: favEntries.length,
        created_on: user.createdOon,
        reminder_set: user.reminderIsSet,
      });
    } catch (error) {
      next(error);
    }
  }

  static async updateProfile(req, res, next) {
    try {
      const { push_sub: pushSub, reminder_set: reminderIsSet } = req.body;
      const pushSubString = JSON.stringify(pushSub);
      await User.updateOne({ email: req.authorizedUser.email }, {
        pushSubString, reminderIsSet});
      res.status(204).json();
    } catch (error) {
      next(error);
    }
  }
}

export default userController;
